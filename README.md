Les fonctionnalités on peut donc faire une recherche par titre.
En bas on peut naviguer dans les pages de la recherche.
En cliquant sur un poster on trouve quelques details en plus.
J'ai choisi de passer l'id du film dans l'url et donc de refaire une requete au moment de la 
selection du film car si on utilise la fonction "précédent" du navigateur on retrouvera notre film.
J'ai de plus fait un petit state pour l'app afin de stocker le mot recherché. Cela permet lorsqu'on revient sur la page
de recherche d'avoir encore la recherche présente. C'est pas le mieux pour un projet plus
grand il vaut mieux utiliser redux.
J'ai fait très simple niveau design. 
import React from 'react';
import './App.css';
import {Switch, Route, BrowserRouter} from 'react-router-dom';
import HomeView from "./components/views/HomeView";
import DetailsView from "./components/views/DetailsView";

class App extends React.Component {
  constructor(props){
    super(props);

    this.state= {
      previous_search:""
    }
  }

  render() {
    return <BrowserRouter>
      <Switch>
        <Route path="/" exact render={()=>{
            return <HomeView {...this.state} setAppState={this.setAppState}/>
          }}
        />
        <Route path={"/details/:id"} component={DetailsView} />
      </Switch>
    </BrowserRouter>
  };

  setAppState = (label,value)=>{
    this.setState({
      [label]:value
    })
  }
}

export default App;

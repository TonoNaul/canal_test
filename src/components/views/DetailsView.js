import React from 'react';
import {getFilmFromID,getImageFromPosterPath} from "../../API/filmApi";
import { IoIosReturnLeft } from 'react-icons/io';
import { Link } from "react-router-dom";
import ReactLoading from "react-loading";

class DetailsView extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      name:"",
      budget:0,
      vote_average:0,
      poster:"",
      loading:true
    }
  }

  render() {
    return <div>
      <Link to={"/"}><IoIosReturnLeft/></Link>
      {this.state.loading?
        <div>
          {this.displayLoading()}
        </div>:
        <div>
          <div>name : {this.state.name}</div>
          <div>budget : {this.state.budget}</div>
          <div>vote : {this.state.vote_average}</div>
          <img src={getImageFromPosterPath(this.state.poster)}/>
        </div>
      }
    </div>;
  }

  loadFilm=()=>{
    this.setState({
      loading:true
    });

    getFilmFromID(this.props.match.params.id).then(data =>{
      this.setState({
        name:data.original_title,
        budget:data.budget,
        vote_average:data.vote_average,
        poster:data.poster_path,
        loading:false
      });
    });
  };

  displayLoading=()=>{
    return <div>
      <ReactLoading type={"spin"} style={styles.loading} color={"black"} height={100} width={100} />
    </div>
  };

  componentDidMount() {
    this.loadFilm();
  }
}

const styles={
  loading:{
    margin:"auto",
    height:"100px",
    width:"100px"
  },
};

export default DetailsView;
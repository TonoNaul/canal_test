import React from 'react';
import SearchBar from "../SearchBar";
import ListFilms from "../ListFilms";
import PageHandler from "../PageHandler";
import {getFilmFromText} from "../../API/filmApi";

class HomeView extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      list:[],
      searched:false,
      currentPage:1,
      totalPage:1
    }
  }

  render() {
    return <div>
      <SearchBar handleSearch={this.handleSearch} resetSearch={this.resetSearch}/>
      {this.state.searched?
        <div>
          <ListFilms list={this.state.list} onLoading={this.state.onLoading}/>
          <PageHandler currentPage={this.state.currentPage} changePage={this.changePage}
                       nextAvailable={this.state.currentPage<this.state.totalPage} previousAvailable={this.state.currentPage>1}/>
        </div>:
        <div>{this.state.not_found?"Movie not Found":"Search Movies"}</div>}
    </div>;
  }

  handleSearch=(text)=>{
    this.setState({
      searched:true,
      searchedText:text,
      onLoading:true
    });

    getFilmFromText(text,1).then(data =>{
      if(data.results.length!==0) {
        this.setState({list: data.results, totalPage: data.total_pages, onLoading: false});
        this.props.setAppState("previous_search",text);
      }
      else
        this.resetSearch();
        this.setState({
          not_found:true
        });
    });
  };

  changePage=(type)=>{
    this.setState({
      currentPage:type==="previous"?this.state.currentPage-1:this.state.currentPage+1,
      onLoading:true
    },()=>{
      getFilmFromText(this.state.searchedText,this.state.currentPage).then(data =>{
        this.setState({list:data.results,onLoading:false});
      });
    });
  };

  resetSearch=()=>{
    this.setState({
      list:[],
      searched:false,
      currentPage:1,
      totalPage:1,
      not_found:false
    });
  };

  componentDidMount() {
    if(this.props.previous_search!==""){
      this.handleSearch(this.props.previous_search);
    }
  }
}

export default HomeView;
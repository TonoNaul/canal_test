import React from 'react';
import ReactLoading from 'react-loading';
import FilmPoster from "./FilmPoster";
import { Link } from "react-router-dom";

class ListFilms extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading:false
    }
  }

  render() {
    return <div>
      {this.props.onLoading?this.displayLoading():this.displayFilms()}
    </div>;
  }

  displayLoading=()=>{
    return <div style={styles.loadingDiv}>
      <ReactLoading type={"spin"} style={styles.loading} color={"black"} height={100} width={100} />
    </div>
  };

  displayFilms=()=>{
    return <div style={styles.listDiv}>
      {this.props.list.map((item)=>
        <Link to={"/details/" + (item.id)}>
          <FilmPoster
                    key={item.id}
                    style={styles.poster}
                    poster={item.poster_path}/>
        </Link>)
      }
    </div>
  };
}

const styles={
  loadingDiv:{
    width:"100%",
    height:"600px"
  },
  loading:{
    margin:"auto",
    height:"100px",
    width:"100px"
  },
  poster:{
    width:"220px",
    height:"300px",
    display:"inline-block"
  },
  listDiv:{
    overflow:"auto",
    height:"100%"
  }
};

export default ListFilms;
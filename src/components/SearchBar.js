import React from 'react';

class SearchBar extends React.Component {
  constructor(props){
    super(props);

    this.state={
      search:""
    }
  }

  render() {
    return <div style={styles.searchBar}>
      <input value={this.state.search} onChange={(e)=>this.handleChange(e,"search")} style={styles.input}/>
      <button onClick={this.launchSearch}> SEARCH </button>
    </div>;
  }

  handleChange=(e,type)=>{
    this.setState({
      [type]:e.target.value
    });
  };

  launchSearch=()=>{
    if(this.state.search!=="")
      this.props.handleSearch(this.state.search);
    else
      this.props.resetSearch();
  }
}

const styles={
  searchBar:{
    backgroundColor:"black",
    boxShadow:"1px 5px 3px black",
    height:"40px"
  },
  input:{
    marginTop:"10px"
  }
};

export default SearchBar;
import React from 'react';
import { FaAngleRight,FaAngleLeft } from 'react-icons/fa';

class PageHandler extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return <div style={styles.pageDiv}>
      <span >
        {this.props.previousAvailable&&<FaAngleLeft style={styles.pageIcon} onClick={()=>this.changePage("previous")}/>}
        <div style={styles.pageText}>{this.props.currentPage}</div>
        {this.props.nextAvailable&&<FaAngleRight style={styles.pageIcon} onClick={()=>this.changePage("next")}/>}
      </span>
    </div>;
  }


  changePage=(type)=>{
    this.props.changePage(type);
  }
}

const styles={
  pageDiv:{
    height:"20px",
    position:"fixed",
    bottom:0,
    backgroundColor:"white",
    width:"100%"
  },
  pageIcon:{
    height:"20px"
  },
  pageText:{
    display:"inline-block"
  }
};

export default PageHandler;
import React from 'react';
import {getImageFromPosterPath} from "../API/filmApi";

class FilmPoster extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div style={{...styles.posterDiv,...this.props.style}}>
      <img src={getImageFromPosterPath(this.props.poster)} height={"100%"} width={"100%"}/>
    </div>;
  }
}

const styles ={
  posterDiv:{
    margin:"5px",
    boxShadow:"0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)"
  }
};

export default FilmPoster;
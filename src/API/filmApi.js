const API_TOKEN = "92b418e837b833be308bbfb1fb2aca1e";

export function getFilmFromText (text,page) {
  const request = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&query=' + text +"&page="+page;

  return fetch(request)
      .then((response) => response.json())
      .catch((error) => console.error(error))
}

export function getImageFromPosterPath (path) {
  return 'https://image.tmdb.org/t/p/w300' + path
}

export function getFilmFromID (id) {
  const request = 'https://api.themoviedb.org/3/movie/'+ id +'?api_key=' + API_TOKEN;

  return fetch(request)
      .then((response) => response.json())
      .catch((error) => console.error(error))
}